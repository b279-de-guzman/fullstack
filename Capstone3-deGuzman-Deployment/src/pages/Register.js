import { useState, useEffect, useContext } from 'react';
import { Form, Button, Overlay, Col, Row, Container, Image } from 'react-bootstrap';
import { useNavigate,Navigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function Register(){

	const { user } = useContext(UserContext);

	const navigate = useNavigate();

	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [isActive, setIsActive] = useState('');

	function registerUser(e) {

	    // Prevents page redirection via form submission
	    e.preventDefault();

	    fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
	        method: "POST",
	        headers: {
	            'Content-Type': 'application/json'
	        },
	        body: JSON.stringify({
	            email: email
	        })
	    })
	    .then(res => res.json())
	    .then(data => {

	        console.log(data);

	        if(data === true){

	            Swal.fire({
	                title: 'Duplicate email found',
	                icon: 'error',
	                text: 'Please provide a different email.'   
	            });

	        } else {

	            fetch(`${ process.env.REACT_APP_API_URL }/users/register`, {
	                method: "POST",
	                headers: {
	                    'Content-Type': 'application/json'
	                },
	                body: JSON.stringify({
	                    firstName: firstName,
	                    lastName: lastName,
	                    email: email,
	                    password: password1,
	                    mobileNo: mobileNo
	                })
	            })
	            .then(res => res.json())
	            .then(data => {

	                console.log(data);

	                if(data === true){

	                    // Clear input fields
	                    setFirstName('');
	                    setLastName('');
	                    setEmail('');
	                    setMobileNo('');
	                    setPassword1('');
	                    setPassword2('');

	                    Swal.fire({
	                        title: 'Registration successful',
	                        icon: 'success',
	                        text: 'Get ready for your new experience!'
	                    });

	                    // Allows us to redirect the user to the login page after registering for an account
	                    navigate("/login");

	                } else {

	                    Swal.fire({
	                        title: 'Something wrong',
	                        icon: 'error',
	                        text: 'Please try again.'   
	                    });

	                };

	            })
	        };

	    })

	}

	useEffect(() => {

	    // Validation to enable submit button when all fields are populated and both passwords match
	    if((firstName !== '' && lastName !== '' && email !== '' && mobileNo.length === 11 && password1 !== '' && password2 !== '') && (password1 === password2)){
	        setIsActive(true);
	    } else {
	        setIsActive(false);
	    }

	}, [firstName, lastName, email, mobileNo, password1, password2]);

	return(

		(user.token !== null) ?
			<Navigate to="/" />

		:

		<>
		<Container fluid className=" w-100 m-0 p-0">
			<Row className="w-100 m-0 p-0">
				 <Col className=" vh-100 m-0 text-light d-flex flex-column align-items-center justify-content-center text-center mt-5 p-5" xs={12} md={6} lg={6}>
				  <Image className="vh-100" src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxISEhUSExMVFhUXGBgXFxcYFRcYFxcZFhgWGBgWGRcaHSggGBslGxcXITMiJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGhAQGy8mICYtLS0uLS0tLS0vLy0tLS0tLS0tLS0tLS0vLS0tLS0tLS0rLS0tLS0tLS8vLS0tLS0tLf/AABEIAOEA4QMBIgACEQEDEQH/xAAbAAACAwEBAQAAAAAAAAAAAAAABQIDBAYBB//EAEIQAAECAwQGBwUHAwMFAQAAAAEAAgMEERIhMUEFE1FhcbEiMlKBkaHBBkLR4fAUI2JygpKyB6LCM5PSFUNTc/Ek/8QAGgEAAgMBAQAAAAAAAAAAAAAAAAMCBAUBBv/EADwRAAEDAgIGCAIKAgIDAAAAAAEAAgMEESExBRJBUWFxEyIygZGhsfDB4QYUIzNSYqLR0uJCcrLxFYLC/9oADAMBAAIRAxEAPwD4ahCEIQpvYQaHFM5KTs9N9xF4GNKCtSBUnK64gGqxzrwXktw+q34uvzN6ELKhCEIQhCsbDJQugE4BVqQC2QpKqcyejKULhWvVBz3ndzSnStCuw6PlkF8guebAJVwkyuhjCGw0ezHNoIHEX07lnixC00DBtBraBG3JR1pDkPgn9BRx9t9+WPmMPNK2yKsGj9y169+0Dg1qNc/tlGrJvC509CMmuPc3+SyHR+5VukFv1j+2V7r4m0ftb6o1ZN4R09Cc2O/T/JKnSZVToBCd/aNrAfL5IrDPab5jyRrSDMeGK70NFJ2JLc7jzOHmkFgrxzaGi6+DooNFs0qK4E3ZZAkZ35EJHPSYtEtwrddTyUmytKTLo+RuIxG9KkKx8MhVpqokEGxQhCELiEIW6BL0p0bTz1W7B2nfD6IhYULdFaAbERgYdorcdtKmo4LNFhlpofrYRuQhVIQhCEJhIwRXpAVyBFRTPi4CtxWKG6hBWqamA4AACtACQKVphd6+iEKU1M4saaitSRWmJNkbrzfiVgQpNFUIUVIBesbVMZSTqoOeGjFWKenfM6zVnl5UlNIUqGirqgc+AVhe1mF7vJv/ACVFkk1N5SgHSYnAK+6SCk6rBrO8hz3ngPHMK4zHYBG9xqVfDiFr2Ret0QDxAoRuNKLO2GtMriQ0W64tH1cd6aGtYLjx+aoSzzVLgHEk7APg0beQumUexEaIkQEMFbIr0nE8MMEpi9I4UAuA2DHHPimP2NzqWnBrRcGt6RA35A+Ks+ww2C04fqiOLR4CyCkOrIm4DHktSD6O10o1nANH5js5AHzsk5aBivA5m0eITN2kJVh68Np/AwnzDVE+0sHtv7mqP1mQ5Rn33Jx0JTMNn1bAd1h/P4JdaZtHiF6KHAgrePaaD24n7VP/AKpLRD0nsd+dh9W0R9ZkHajPvuQNCUrjZlYzlYfz+CWmEtMnLgGppXE1yAzB455Y8GEOWgvvaATthuLqfpwHgvJ6SLjVlkbaizU7jh5BSbWRHPDmlz/RytjxYA8flPwNvIlKI8Q0DA4kDO8VupcMhzxKgJg+8K+R79qujQnNue0jjf4HAqowk8hrxfP3vWSySaleWi7TtBFvFpw8l4+Xa4dG8eY7krjylEwLaXq0RQ65+Pa+KVquZi3EeavtngqhqygNdv8A8T/Hvw4hc+RRQTeckqJY9hCax4cMFRqaV8DrOCrTWHNaqLrKVa8YjOtKkd4wSuivgxqCyRaab6YX7QcipqsnM6YbgIsVuIoxlaE51NOPckkaKXGp4ADAAYAImI5eangBkAMAFShCEIQhCEIQhCFNjarwBMpGVqoPcGi6sU9O6Z+qFdIydePP58+eiJE9xuHvHbw2BEZ/utwzdt3cF6xlePP5/XFbGa3WcrtTUtiaYIDwJHoOG87css62MWljO+uAzPyVkCAXGgFT5Ded3PxIYuMOA0vcaUxd7zvwgbPw+O1cmqBHgMXblLR2iX1QMjzqRDNx4Z2v5k4DjkqZfR5N7zf2G+rm8m7MVGb0xBhCwKOONhlLIwxdhzS7XzM6/UwGGycQMxte7IeXFdp7P/07gw6PmTrX9gVEMcc3+Q3JQp3yHWlPcPfver0ml6ejBi0ewcXnEnxxPC+H5bWXGS8zOzZsS8N36G4fmeer5J3I/wBNZuJR0eKyH3mI/gadH+5fToDGsaGMa1rRcGtAAHAC4Ky2rbGNYLNFlhVFVPUm8zy7nl4ZDuAXFyv9LpUUL4sZ5zs2WDwo4070wZ/T3RwxhPPGK/0IXSW0W1JV1zj/AOn2jj/2nDhFf6lYZn+mEm69kSMzdaa4eBbXzXY20W0IXzDSH9MI7elBjMfTAEGGe69w8wkc3/1CTNI8N9n8bbTe6IM+BX2u2ovIIIIBBxBvB4hRexr+0Lp0FRLTm8Li3kbeIyPeLL5DI6dhROi77snJ3SYe/wCKvj6OGLDZOw1sngcuS6fT/sBLR6ug/cRPwisM8We7+mnArhJmFN6Pfq4rehlmx29jsju8QqrqZzDrQm3D38VvxabiqW9FpFgcNjgLEeH/AM23apVkSHfZcKOGIPNZnsTqXmIUwy68DLB7Du8McFkmpUtxNa4HJ3dkdynDUB51XCzt3v0VPSOiHU7BPE7XiOTt3O3rvztgFhhxqdF17f4qubkxSuNcDt38Fc+Hn4KMOLS53VP9u9TfGb6zc0ilqmlvQT9nYfw8P9dn5eWSKK0g3qtOJ+USpzaKcbw8XVeqpnQPsVBCEKaqoQhCEIQhWMbUoXQLmwWiTg1NU4f0BZHWP9o+arkoYaLZwb5nIKLak1OJVcfaOuch6rWkd9UgDG9t3kN/fkO/gV7DYtsvBLiAMTy2naOfiRXDbTKtcBtPwTVzmQIZc44dY9pwwaN2Q3X7UVE3Riw7RyXNE6OFU8vkNo2YuOXG1/MnYOYRNTLIDKnH3W5xDtPly2ArNCaDj6RiW3kthg9J9Lh+BgzPLPfHQOi4k/HL31ENvXOwZMb9XY8fqctDaxoYwBrWigA+seaIINTE4uOZUtKaUNURHGNWJuDWjDLIkeg2c17orR8KWhiHBaGtz2uO1xzK2axZdYjWKwsdatYjWLLrEaxCFq1i9D1Q0FSpvXLqQY5WmIvNYq6b1FwOSLhGo7crtYjWLNbXmsXVFatYqJ6WhxmGHFaHsOIPPcd4UNYjWIQvmftN7MRZJ+vgEuhbfeZ+F+1u/wAcq36Mn2R2UIoadJn+Q3cl9FeQQQ4AgihBFQQcajYvmHtZoB0pEExBrqyf9t3ZP4Tl4cUTwCQcdhWpovSj6J9jjGe034jj5EYEZEezkuWnG1XA9oZ/qHzS+I1OZOZbMQ64doZsd2m8/JYI0Iglp6wx37wo08xd1X9oe/8AtO0vo5kBbPAbxPy4Hdy3bRYg5Y54BtCwcfd+CWTsCi3xGKcwLbbWef5vnipPGo7WGR9+ahTu+swmF3aaMOI+Xpyx55CtjMoVUnhZLmlpsUIQhC4hbpCHVYwF0uiJUCjiK5hva3ndzSpXWar2j4RJLc5BVTJwZsNo8T9ea9gtV8Vj2n7yG3pbA0V4EYEfVV79nrRrTW1c086jaNik0BjcdiTPK6pmLgMSQAPJo9O9bNFwa9M49Vn8XH/H9yUzz3Tcw2DC6taA5HtPO4chvTTTcyIMGy3E9BgOQuqf23fqWz2D0dq4ZjuHSiXN3MHxPkAqtODI8zHu9+9q3tLyCip2aPiOy7zvOfmceWqMsF1OjJRkvCbCYLmjHMnNx3la9Ys2sRrFdXmVpMVGsWbWItoQtIerYTlVDFkWjjfv3ZY51UWxQa0XHZJkXaWoxEW1m1iNYoK1ZabaBEWbWI1iFyy0RHKjWKEWJcow8rqk4D1Kk1V5e0rdYjWKuI4tNHNpwA8RS5VudT6xUkpaNYqpmG2Ix0N4q1woRtBVesRrEIXzSZgukJosNSw/3MOB4jmE50lCDmiI33Rl7zMa92P7k09sdHa+AXAdOHVzdpHvN8L+IC572XnKssGtpmH5HV5H+QVOqaWkStzGfL3gvSaDmbOx+j5uy8Et4Ozw2fmGy4O9ZowUJc0NMnXd+X1vWmZgWHFuQw4G8fDuWSJDJNBWuVMVZwkbwKwx0lJPj2mHEcsCORyuss9LkmgFTlRLHCi7KLAAZrXUq4X3kipuI6ORNbxgad/JzZq8nIm64C7gLlGF2FirGkY2iTXZkcvgs6EITVnq+WbUrpmxSyGx7bzc0+JND/aKrnpFt6cxohaWU2GoOBDsj4JDsZAFqQ/Z0cj9pw8cD5XTjXNiMESIKMBNG1qXHDwxuVUibb3PoA1oo0DAF/rQY/jS7WE0yAwAwG3FNdH9CFaOHSe7gLh5NaoVjtWKw24Kx9HoGy1oc7JgLvgPW/ckelKzE02C3AEM9XHu/wAV38IBrQ1twAAA2AXALhvY+GXx3xXXloJr+Jxx8LXiuytqwxgY0NGxZdXUGonfMf8AIk92zwFh3LVbXmsRo5/3jePoU3m9JNhkA1wrdRQfIQ7VAunU9IySIyvk1QDbK+7iN6U21dLxADeR30Ipn3q7TMICjwMTQ76g3pjpDSFihcMbrgNij01wCBmnf+MLXyNkeGhtje2w3ttFsvdkldGyBu509MV7Bi3q6dnmxQ1jag2hjTeNu9a3zLIIa2mOzmV0yGwFsTsXI6Jms5wlGo23WttOy1/isdtFtXaUhigeM8fQq7SJq1o2uA8QVDpRhxVl1C5peCezY5Z3y24eax20W1ujTLYQAoabvrFUxZiGHhwvxrRAkJ2LslG1mBkFxa43X78fJYo0VTZM2XB4vFAPIVG4pkycBZbvpQnfcqpOdD4hIrSyAQeJ2cV0TGx6uXvclP0awvaBKCXZYbN/aUI0w0gPeMui2uO8lL3xa/WCZx9MsDiCHXXYCl2y9eaMjhzojhgS3Hg5d6UgElqW3R8bpGxslBJJGWVgTv3iyV6xe201g6Ra9xZQ540oaJRPssPIGGI71NkhJsRZJqKMRxiRj9Zt7ZWsc95Ura+fRm/ZZ0gXMJ7rLxj3H+K7e2uU9uYH+nE4sPMf5KbmhwIO1VYpnQyNkbm0gjuxWvSsHquGIqw8MRyd+5ZYDGjGzga1FRShpQjfStL1uhxbcvaxLodeLmEHmxLp2NbNaAZ8TtVWjJ1C07Ctz6RxNFS2dnZkaD3jD01VDXWmuZUkMNQTia3VPgLkgnGUKcyvW4gj19Es0gxMHVlPFVHfaULTtaSPDEeRCwIQhPWUmOjmrfMdc7g3+NFk0Zktcbru4pLfvDyWpNhQsG9w/wCLlYDQVTPSRsSrtrYbWeJa0pW/qHgeSY+1f+i787UmpxfGOP7K9oMllNWSDMMFvB/yWf2NjAiIPe6J44j64ro9YuA0NO6qKHHA9F3A59xoe5dvrK/WKuLzqZaMf96zj6FOomrLwHAF1Lq1wv7tq5vRsUCI0k0AOJ4FbNJTo1zHtcCAMjXM1Hgq0rS6QAbitugnbDSOc6x64wNsurcgcMwVdpyaJcGUoBfxr9FM56YY294urddVJdPRWOsua5pOBoQbsR6+KYzEWC80c9hp+IfFKdbVZcHb8FoRh/T1IDm36ltbK3WwwxwHms32hjozCzC/Kl9/yV88GmM0PubY20vqc1h0gYTQ10KzaDgbnVwrv20WozEGO0F1LsiaEbRvCkTazgDaxHHaksZrdJC8sLy5r7f4nIEZHdjgcwrJ6E0MqMbqX1qFdpSJRgO8cisE7OsfDo0i40puAxpsWidnG0aQQSHA0ruKgNbq33lOf0NpdQgAtbl/7XsBnyWiFpFpudcd+HiqNKQw0WxdlTLcV6+JBiUc4iu80PeEv0zPB1GtNRWpK7EOuLA8UVr7U7zI5rstUjMn3uKYSz//AMv6X/yKz6Af0ncPVVy8y0S1m0K0ddUVvcaXKnQkw1rnWnAXZmmamQdWTn8VVa5vT0mIwYNuXV2ppFnoDSQQK59A48VV7Ovuf+n1XjmyxJJLKnHpH4qvR8xDY6ILTQ2rbN42HBQw1CADsz+FlaDXirifI5lgXAat74tJ61+XiUxlhDNSwCoqDjWuJxXPzEyXuLjdXLZuV+iZwNe8FwAN4JN1QfgfJZJ8ttuskEVrca43806Juq8g7s1m10vS0sbm2HWILRbPGxtnv5ko1iT+1bx9nvxtNs8b/SqYW1y/tTO2nCGD1bzxOHgOasLGTP2WfWC2uTyPHpeqxFlkAbLvC5XeyR+7cPxjkVGOOlE/O/8AmVTgwnkHevSaUbr6MpJNw1fL+qzQrnt4jzuWXSjcVpOI4jmqtKi93FNk+8CoUnWpJBxHmPkkqEIT1k3TXRmS1xuu7isGjnJhM/6h/T/EFIb94eS1Z8aFh/MP+LlJ3UPA8kw9rB9y784WACoptuTLS4L5Zzsyxj/NpSqnCSN3H9le0IC6lrGD8A9H/JcSul0BpCo1bzh1TtHZ4hc81uZw+rggxDWouphTJXF51d0YlVHWJJo3SlujXmjtva+aZ268eaELRrEaxZbaY6CsuiODgCLJxFc2qL3arS5OpoOnlbEMLmyo1iNYmumpRpZbY0CzjQAVF1/d8V5JsaZS0Wi1YffQVuc6l6UKgagdxstA6HkFQ6AkYNLwbYECw9TbuS+BFvWkPXmhZMP6bsBcBtO07k118EusUbXDq57KqEsoDrAXT6HR7pIWyPcGhx6t9v8A3s2nOyWayvHmskeLet8/DEJwd7tbwb8MR4K7TkBurLmtALSLwAKg3cyF1souOKjNo6To5DcAsxI3i178rXSfWI1ic6Hl26ppc1pJqbwDndjwS+Xlh9qsEXBxNN1KjmFMTtJcNyrv0VI1kLr/AHhAtbIuFxfu9Fm1iNYtOnntEQNaAKC+gAvPyolltMY7WaCqVTB0Eror3sbX97jgtWsXoiUv8FmD1mnJ5sMVN5yGZ+SkkrRpPSAhsLveOA9eAXIPcSSTeTee9TjzLnutE38hsCiRW8d49RuQhdL7JD7t/wCdvqvI56UT87/5lafZRn3La5vJH9o9FjLrQtbb/G/1VOHGeQr0mkyGaLpGbT1vL+yodiOIVelTe7irGCr28RzWfSjsU2T7wKhSYUkh4jyB/dKEIQnrJW2Qdems17h2jl8ikks6hTtxrDr2T5G74JDsJAVrQ/aUUjN2PgbnyupQU6lGB8PVuu60MnZUmg8C1Y5GXDBbdiNmVBgMBarxFxGK0yUyC9zRgQCMh0LjQZClm7cVCsbeO42Yqz9HJxHWhjsngt78x6Ed64l5OBupdTYqk19opexHdsf0x34/3VSpWGO12h29Y9TTmnldCf8AEkeGR7xihM5PShFz7xtz79qWIUkldQIwdeCDwTb2bf8Aen/1nm1cNCjubgfguv8AZCPWL0h0yw2RlSrekdm4fRTP925aGib/AF2K2/4FdFLzQMWJCPu2XDeHMFW+J81WIerl4jcbLYlOBLiOaU6YmHwJovIaR0bwG39BoItAVqms7Fa6A97SS10NxG3A3EZHJUpGFuqRkbeK9TR1LZ3TNd24zIAd7C4nwFgO4E43U9Av+5h8XfzKSSsUksOdoeNUaA0kGVhvNKmrCcAReQdiat0exr9bfjapUWKnP1U3O6J79bbl5/uq0EJrqWnMJHUAa4bRYNF/03G8HBe+0L/uT+YfXhVew362W3uhkd7buYSTTukhEIYw1a28nInC7gK+K1+zMxVjmdk1HA/Mea4Yy2AE5g3UmVUc+lHxNN2uZq8yLnDkC4Jo6PY1EPbd+xnxIU4cGkd0Taxo76mvk1q572gnLMZpF+rDT31tfBdBNR7LXvBBp1d5J6PnRLe0hrT+IfFXaeaOWWZrsonAjgAyx8CHLm52MYkVxF9XEDuuHkFntUO1ShPZCZaJBNMQLQobtoBFK1GJqCEhntIOiOJFQCcK+VVqAWFgvBPkdI4vdmSSeZxPmt83pINuHSd5BJosUuNXGpVaF1RQpA0UVt0ZK6yKxmRN/wCVt7vIFcJAFypxxukcGNzJAHM4DzXVwG6uXAF1IdRue8/FyVvTXS8Soa3tOJPAfMjwWOHBNbLRV58GDad/LjhVowdQvOZPvzut76RyNFQynZlG0DkTs8A1Y5YdLg1x8iPVLtIuT6w5gcIrRfgQGg73AgX965/SQo6mOYO0HApmcnJUj9nQgfiJPw+CwoQhPWWpsxC6nQxAucaE0Fai6t/cdjlyzHUIKbQJ8usjYKVNKkZCuwfNKlbcYK/o+XUkscjmtcSNeQ3Ct91Kmpupk0HAKyDFsua8ZGvjiPCqpmReH7eYz+tq9glSBD247fZSJGPpZ7NOLSCDyxafCy3+0UoIkK228s6Q3sdieR7lxq7rRkbFhxbeN4reO4+RC5jTMhqYhA6rr2cNncq1M4scYXbMvfmt3TcTamKPSEWTgA4bjl/XubbNLEIQri80hMGxi0sitobIDXcbxQ92B3LAQrIUUtNRwIOBGwhCE+fNNLGxogGdhgNanMnw7koiTznGppuFKADYKKiNFLjU8ABgBsAUGiqFwgHNb4UzaOC1siwxl5JbKHFaEt7jey0KeFhaHEAlbHTDDl5LJMRgMiQvFTMm7vKixxBXamJjmEkIE043ABapVwbe40OeBBFMKZHAkZhLYT7JqrpuZMQ1O6poKuNMT9XJyzi0blGPFr0RWyMK4nKp308FnQvQELq8QhCEIXVeyslRpinF3RbwFanffd3FIZCUdGeGDiT2RmV2E68MYGNuqLLdzRia8Lv1KnVOLrRNzPovRaCgbFr183YjBtxdw9BvJG0JfORrTi4YDojgPnU968dNGG/WtoQRQ7zQVB2GoqqIhRKONrIjEg4GmHnTzVkWjZbYFjOdJVzkntPPr8APIJvpKYa2GHRGtLy25mIBN9TtXFzEUucSfrduCYaVmS4kk3/VyUKMQwuU/SDxrhjchghCEJqz0K2C+hVSEFdBINwuilnhzbP0Dkq4Zpil8lGomkcWhbH6viq7eo7VOR9VrTt+swCVvaaMeLf658rq+G83EGjheD6HkmU1AZMQqYA4bWPGR+sCkkNy3ykyWmu24jtDLv2Hu4cqIS8Bze0MvfopaI0iync6GfGJ/aG7Zf8AfxGVjy0xBcxxa4UcMQvA2l57h6ncu00jo9kdgIpUdR3f1XbuS42YhOY4teCHDEKUE4kHEZhL0pot9E+4xjPZd8Dx8iMRtAqJqooQnrKQhCEIV7XVvHW5/PnxxsbMjMLIrutx5/Pnxx4WgpjJXM7KudMjJUudW89w+slECl57ht+SiTVAaAuvmc/NBNVFCF1KQhCEIVvW48/nz44jGkkACpNwCGMJIAFScAF2Oh9FCELbqa2l5yaPjtP0UzTCJtznsC0tG6NkrZNVuDR2nbvn6Zle6Mkmy8M2uti93Jo5cTwWOPFLnFxxOXZGQ+t6tnJm2buqOqP8j6BYIj0uniIu9/aKtaX0hHIG0tNhEz9R38Rx2m5xwKhGerIv3bbPvG8/8e74ol2++cB1ePaS+fjqbzru1Ak0jBTwmd+ZFm8tp79nDeCskxEqVQhCeMBZZbnFxuUIQhCihCEIQpg0TSQm6JQrIb6FQewOFlZpal0Dw4J7Hh06Tep/HdvUobqeipkZkUvvriNvHcrIsOnSbe3+Kgx5vquzVmrpWkdPB2do/DxH5eWXJb5WaLcL7WIyd35Hf3bKaZqVhzDb8sHe8w9l3w8NqTMetUKJfUGjtu3cdqhNT6x124O9+7p+jtLGBhgnbrxHZtHLhw2HEEHNNpDRsSCekKtyeMD8DuS5d1Bn2u6MQBuW1js764d/isU97OsN7Og7Ze5nxb5qDaotOrKLHf7+CtzaEZO3ptHvD2/hJxHDH0dY22lckhMZrRUaH1mGnaF48sO9Llba4OFwbrz80UkLtWRpadxFvVCsApee4bfkgCl57ht+SiTVdS1YTa/Nz+fPnShXdbjz+fPjiIVKELbK6PixL2MJG3AeJuXCQ0XKnHG6V2qwEncBc+AxWJbJKSfGNGDicm8Sn8h7NsF8R1s9ltze8i8+SYxJtjBZaAae625o4nDwqVUdVax1Yhc+S34NBdG3pq54jZuv1jw+QuTuBVWj9Hw5dtqvS9555NH0eSonJkuFMG5N9XHkFVGjFxtONTlsbwGSyxIilFT2OvIbuSdIaXEkf1albqRDxdz4HbmTtOJCsiPrx5/P641QodrHqjE+iIUMuvwbmfgiamRSgx5/PnzZJJ/i3P0+ap0lKCOmm7G78Xy3nuCqnZq6gu2JO91SpRYloqpSjZqhKq6ozvvsQhCExVEIQhCEIQt8tBNW0AL3dUZNHaPoO/iIWBetKazkOLCpbIe04513EkV4LDMQgKOb1XYVxFMQeCEKDIpBqmcnOpOptdRQewPGKtU1U+B12roDCDr2Y9n4KpkRYJacomQjtf1rzt975pQe5mDsQrrqeGp60JDXbjkf29OWN5iKrpeYczqGm7FvgfSizGARh0hux8FCHETerIN4VC09JIDix2/LwIzG+ydy2kbxVptHsGvkfiVKNDl4h6WqcTtBa4njcSN4VEu1rG2iQTjUd4AFcMCa4OpTjjjx7Tidv1hlwyVd1Gy92khbEX0jqdXUna2Ru24sT4YfpWuJ7OQTfYiDgf8AlVZz7MQ+0/wHxVLCBhdwu5KbYzu2/wD3H/Fc6CcZSeKYNKaMf95SAf62/qpj2XZ/5H/tHxV7PZqDjZiHcT8AsxjO7b/9x/xVcQh3Wv4380dBOc5EHSei2DqUt/8Aa39k1EKWh3AQw4bavcN/vOXkXSjTg1x3k0HhjySoOWmXYahrb3m/cwUrXjTw44dFGy93kk++/wA1CT6R1GrqU7GxjgLkcsh+leR5p77nOFNjbh8+9Z9ZRaJ10SGQIlHAjx76VqNqzvlTd2SKgm668dXbcrFmRjcFjudPVyXJL3ePyA8AqYkRWNg5vuHZz7+yjWNh9W93aPp2UvmZ2qWXufg1XWUsNONacgn8Iy7zt5ZcwVpm5sYDwSmJFLlFzyVBTZGGqpVVb5zjkpk148/moIWmBALr7w0Ymlabv/qYqi8lpcvrTLHvuAG8lUEJzHjNYwBt+zYReCcLwTWozupuSoQhCEIQhbmRCAyIw3sFDtF5v3toaLCrIcQtNQb/AK8UIT104DDbFihpxsMGZFxJr9BJI0YvNo+AuAGwDILyLFLjU8BkANgGSqQhCEIQhCuZFIVKEWXWuLTcJpLTpwTqWmmg/eY3Gt4dS42agVANMQuTY6hqtUxOOdQVNBtvJ3k5pRhGYV+PSMgbqPxG75J5Gax1zHlorWhpedpI5ZKky7tx4H4pMyaIVzJ5RtK3bdM1qGTNpaeBt5G48AAmOrePcPh8F5U7D4FZ26Q3qY0ke0UdJJuXfqlI7KQ+AP7Kyp2HwK9DXn3D4FVHSR7RUHaR3o6STcj6pSNzkPcAPiVrEu78I4uHotjQGObFt1oA0gC4ECl+40uPwSF88oiecDUcNoI2EZo+0PBcvQx5Au5n9rLrZjSDGtbEIbaI6DcaA53rm5vSRcSSb0vix3ONSfr0VCkIhmUt+kHW1YxYcMFdEjkqlCE21slRc4uNyhCEIUVfLwC80H1u8ls1wY2jm30oBeAbxUkVuNW0NMQsEJ9koixC4lxNSUIXkR5JqVBCEIQhCEIQhCEIQhCEIQhCEIQhCEIQhCEIQhCEIQhCELhQhCEIQhCELqEIQhCEIQhCEIQhCEIQhCEIQhCEIQhC/9k=">
				  </Image>
				</Col>
				<Col xs={12} md={6} lg={6} className="d-flex flex-column align-items-center justify-content-center mt-5">
				<div className='w-75 my-5 py-5 shadow-sm shadow-lg rounded d-flex flex-column align-items-center justify-content-center' id="reg-BG">
				<h1 className=" text-center text-light" id="text">REGISTER</h1>
				
				<Form className="w-75 text-light" onSubmit={e => registerUser(e)}>

				<Form.Group className="mb-3" controlId="firstName">
				    <Form.Label>First Name</Form.Label>
				    <Form.Control
				        type="text"
				        placeholder="Enter first name"
				        value={firstName}
				        onChange={e => setFirstName(e.target.value)}
				        required
				    />
				</Form.Group>

				<Form.Group className="mb-3" controlId="lastName">
						    <Form.Label>Last Name</Form.Label>
						    <Form.Control
						        type="text"
						        placeholder="Enter last name"
						        value={lastName}
						        onChange={e => setLastName(e.target.value)}
						        required
						    />
						</Form.Group>

						<Form.Group className="mb-3" controlId="emailAddress">
						    <Form.Label>Email Address</Form.Label>
						    <Form.Control
						        type="email"
						        placeholder="Enter email"
						        onChange={e => setEmail(e.target.value)}
						        value={email}
						        required
						    />
						    <Form.Text className="text-muted">
						    We'll never share your email with anyone else.
						    </Form.Text>
						</Form.Group>

						<Form.Group className="mb-3" controlId="mobileNo">
						    <Form.Label>Mobile Number</Form.Label>
						    <Form.Control
						        type="number"
						        placeholder="09xxxxxxxxx"
						        value={mobileNo}
						        onChange={e => setMobileNo(e.target.value)}
						        required
						    />
						</Form.Group>
						

						<Form.Group className="mb-3" controlId="password1">
						    <Form.Label>Password</Form.Label>
						    <Form.Control
						        type="password" 
						        placeholder="Enter Password"
						        value={password1}
						        onChange={e => setPassword1(e.target.value)}
						        required
						    />
						</Form.Group>

						<Form.Group className="mb-3" controlId="password2">
						    <Form.Label>Verify Password</Form.Label>
						    <Form.Control
						        type="password" 
						        placeholder="Verify Password"
						        value={password2}
						        onChange={e => setPassword2(e.target.value)}
						        required
						    />
						</Form.Group>

						
						{
						    isActive
						    ?
						        <Button variant="primary" type="submit" id="submitBtn">
						        Submit
						        </Button>
						    :
						        <Button variant="danger" type="submit" id="submitBtn" disabled>
						        Submit
						        </Button>
						}
						<div className='d-flex flex-column align-items-center justify-content-center'>
						<p className='mt-5'>Already have an account? <Link to={"/login"}><strong>Login Here!</strong></Link></p>
						</div>
						</Form>
						</div>
						</Col>
					</Row>
				</Container>
		
		</>

		);
}