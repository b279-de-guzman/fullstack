import Landing from "../components/Landing"
import Banner from "../components/HomeBanner"
import Deal from "../components/PromosAndDeals"
// import BestProduct from "../components/BestProduct"
// import Highlights from "../components/Highlights"


export default function Home(){

	const data = {
	title: "BitStore Gamelab",
	content: "On your marks!",
	destination: "/products",
	label: "Go!"
	}

	return(
			<>
			<div className="p-5">
				<div className="py-5">
					<Landing/>
				</div>
				<Banner bannerProp={data} />
				<Deal/>
				{/*<BestProduct/>*/}
				{/*<Highlights/>*/}
			</div>
			</>
		)
}