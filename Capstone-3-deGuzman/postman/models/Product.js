const  mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
    name : {
        type : String,
        required : [true, "PRODUCT NAME is required!"]
    },
    description : {
        type : String,
        required : [true, "PRODUCT DESCRIPTION is required!"]
    },
    price : {
        type : String,
        required : [true, "PRODUCT PRICE is required!"]
    },
    isActive : {
        type : Boolean,
        default : true
    },
    createdOn: {
        type : Date,
        // The "new Date()" expression instantiates
        default : new Date()
    },
    userOrders : [
        {
            userId : {
                type : String,
               
            },
            orderId : {
                type : String,
                
            }
        }
    ]

})

module.exports = mongoose.model("Product", productSchema);