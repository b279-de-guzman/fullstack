import Banner from "../components/Banner"
import Highlights from "../components/Highlights"
import UserContext from "../UserContext"
import { useContext } from "react"
import { Navigate } from "react-router-dom"

export default function Home(){

		const { user } = useContext(UserContext);

		const data = {
		title: "Welcome to Nerdgasm",
		content: "Enjoy timeless classics, everywhere!",
		destination: "/products",
		label: "Check our products!"
	}

	return(
		<>
		{
		(user.isAdmin == true)
		?
		<Navigate to = "/dashboard" />
		:
		<div>
			<Banner bannerProps={data}/>
			<Highlights/>
		</div>
		}
		</>
	)
}