import productData from "../data/products"
import ProductCard from '../components/ProductCard'
import { useEffect, useState } from "react"
import { Link } from "react-router-dom"
import { Button } from "react-bootstrap"
import UserContext from "../UserContext"
import { useContext } from "react"


export default function Products(){
	// Checks to see if mock data was captured
	console.log(productData);
	// console.log(productData[0]);

	const { user } = useContext(UserContext)

	// State that will be used to store courses retrieved from db

	const [AllProducts, setAllProducts] = useState([]);

	// Retrieves the courses from database upon initial render of the "Courses" Component

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setAllProducts(data.map(product => {
				return (
					<ProductCard key={product.id} productProp={product}/>
					)
			}))



		})
	}, [])

	



	return(
		<>
			<h1>Products</h1>
			{/*Prop making ang prop passing*/}
			{AllProducts}
			
		</>
		)
}