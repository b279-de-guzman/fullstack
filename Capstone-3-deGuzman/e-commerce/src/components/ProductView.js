import { useState, useEffect, useContext } from "react"
import { Container, Card, Button, Row, Col } from "react-bootstrap"
import { useParams, Navigate } from "react-router-dom"
import UserContext from "../UserContext"
import Swal from "sweetalert2"
import AppSideNav from './AppSideNav'

export default function ProductView(){
	const { user } = useContext(UserContext);

	// module that allows us to retrieve the courseId passed via URL
	const { productId } = useParams();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [stock, setStock] = useState('')
	const [quantity, setQuantity] = useState(1)
    const [add, setAdd] = useState(true)
    const [dif, setDif] = useState(false)

	useEffect(() => {
		console.log(productId);

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})

	}, [productId])

	function increase(){
        if(stock > quantity){
            setQuantity(quantity + 1)
            setAdd(true)
            setDif(true)
        } else {
            setAdd(false)
        }
    }

    function decrease(){
        if(quantity > 1){
            setQuantity(quantity - 1)
            setAdd(true)
        } else {
            setDif(false)
        }
    }


	const buy = (productId) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/checkout`, {
			method: "PUT",
			headers: {
				"Content-Type" : "application/json",
				Authorization : `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				productId: productId,
				quantity: quantity
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data === true){
				Swal.fire({
					title: "Order successful!",
					icon: 'success',
					text: "You have successfully purchased this product."
				});
			} else {
				Swal.fire({
					title: "Something Went Wrong!",
					icon: 'error',
					text: "Please try again."
				});
			}
		})
	}


	return (
		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>
					<Card>
						<Card.Body className="text-center">
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>PhP {price}</Card.Text>
							<Card.Subtitle>Stock:</Card.Subtitle>
                            <Card.Text>{stock}</Card.Text>
							<Card.Subtitle>Quantity:</Card.Subtitle>
                            <Card.Text>{quantity}</Card.Text>
                            {(add) ? <Button onClick={increase}>+</Button> :
                            <Button variant='danger' onClick={increase} disabled>+</Button>}
                            {(dif) ? <Button className='ms-2' onClick={decrease} >-</Button> :
                            <Button className='ms-2' variant='danger' onClick={decrease} disabled>-</Button>}
							<Button variant="primary" onClick={() => buy(productId)}>Buy Now</Button>
						</Card.Body>		
					</Card>
				</Col>
			</Row>
		</Container>
	);
}