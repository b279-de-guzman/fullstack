import { Card, Button } from 'react-bootstrap';
import { useState } from "react"
import { Link } from "react-router-dom"

export default function UserCard({userProp}) {
    // Checks if props was successfully passed
    console.log(userProp.email);
    // checks the type of the passed data
    console.log(typeof userProp);

    // Destructuring the courseProp into their own variables
    const { _id, email, isAdmin } = userProp;


    return (
        <Card className="my-3">
            <Card.Body>
                <Card.Title>{_id}</Card.Title>
                <Card.Subtitle>email:</Card.Subtitle>
                <Card.Text>{email}</Card.Text>
                <Card.Subtitle>isAdmin:</Card.Subtitle>
                <Card.Text>{isAdmin}</Card.Text>
                <Link className="btn btn-primary" to={`/products/${_id}`}>Check it out</Link>
            </Card.Body>
        </Card>
    )
}