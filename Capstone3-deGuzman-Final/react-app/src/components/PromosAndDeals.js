import { Row, Col, Card } from "react-bootstrap";

export default function HotDeals(){

	return(
		<Row className="my-3">
            <Col className="my-2" xs={12} md={4}>
                <Card className="cardHighlight">
                    <Card.Body>
                    <Card.Header className="th-bg p-3 my-3">
                        <Card.Title>
                            <h2>BUY NOW, PAY LATER</h2>
                        </Card.Title>
                    </Card.Header>
                        <Card.Text>

                           Wanted our products but on a tight budget? Say no more, we got you! Enjoy your products today and pay it later with the different payment plans that we offer! Just choose the products that you wanted and we'll make sure that you will enjoy it.

                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col className="my-2" xs={12} md={4}>
                <Card className="cardHighlight">
                    <Card.Body>
                    <Card.Header className="th-bg p-3 my-3">
                        <Card.Title>
                            <h2>GET SPECIAL DEALS</h2>
                        </Card.Title>
                        </Card.Header>
                        <Card.Text>
                            Have you seen a lot of interesting things? If you avail more than one product, then we can give you a special offer. Orders in bulk are also available! Just check out the products that you wanted and our sales specialists will get in touch with you as soon as possible!
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col className="my-2" xs={12} md={4}>
                <Card className="cardHighlight">
                    <Card.Body>
                        <Card.Header className="th-bg p-3 my-3">
                        <Card.Title>
                            <h2>DELIVERY</h2>
                        </Card.Title>
                        </Card.Header>
                        <Card.Text>
                            No need to worry on how we can our products arrive on your doorstep, as we have our own delivery partner ready to ship all that your need right into your place. 
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
	)
}