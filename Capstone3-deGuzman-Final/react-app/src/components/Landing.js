import { image } from "react-bootstrap"


export default function Landing(){
	return (

	<div  id="landing" className="py-5 rounded text-start">
		<div className="row py-5 my-5">	
			<div id="landing-text" className="col pt-5 mt-5 px-5 mx-5">
				<h1>HERE AT BITSTORE</h1>
				<h3>YOU ENJOY WHILE YOU PLAY!</h3>
				<h4>TURN BACK TIME AS WE PROVIDE YOU THE BEST CLASSICS</h4>
			</div>		
		</div>
		
	</div>
				
		);
} 