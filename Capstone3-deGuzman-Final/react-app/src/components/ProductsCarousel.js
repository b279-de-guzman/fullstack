import { Carousel } from "react-bootstrap"

export default function ProductCarousel (){
	return (
		<div>
		<Carousel className="vh-75 w-100 d-inline-block mb-5 shadow" fade>
		      <Carousel.Item>
		        <img
		          className="d-block w-100 img-fit car-ht"
		          src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTX_Y5pc6H0Z5IOTaeuK7OS17YHDcoWMwJxfA&usqp=CAU"
		          alt="First slide"
		        />
		        <Carousel.Caption>
		          <h3>BRING BACK THE CHILD IN YOU</h3>
		        </Carousel.Caption>
		      </Carousel.Item>
		      <Carousel.Item>
		        <img
		          className="d-block w-100 img-fit car-ht"
		          src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQH0_p7XigPP7UNjWZv_9mjREgGgpRoq7_dDg&usqp=CAU"
		          alt="Second slide"
		        />

		        <Carousel.Caption>
		          <h3>A GREAT WAY TO MAKE CAMARADERIE</h3>
		        </Carousel.Caption>
		      </Carousel.Item>
		      <Carousel.Item>
		        <img
		          className="d-block w-100 img-fit car-ht"
		          src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTuO3X6ZwldHQlXIfWDhpDSDwSkMHQ6naqTQQ&usqp=CAU"
		          alt="Third slide"
		        />

		        <Carousel.Caption>
		          <h3>EVEN YOUR WIFE OR PARTNER WILL SURELY ENJOY</h3>
		        </Carousel.Caption>
		      </Carousel.Item>
		    </Carousel>
		    </div>
		);
}